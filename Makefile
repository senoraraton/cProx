CC = gcc

# Compiler Flags
CFLAGS = -g -Wall -Wextra -Wpedantic -std=c2x

# Linker Flags
LDFLAGS = 

# Output Directories
OBJ_DIR = ./obj
BIN_DIR = ./bin

# Source Directories
C_SRCS := $(wildcard **/*.c)

# Obj Directories
C_OBJS = $(patsubst %.c,$(OBJ_DIR)/%.o,$(C_SRCS))

# Executable name
OBJ_NAME = out

$(OBJ_DIR)/%.o: %.c
	@mkdir -p $(OBJ_DIR)/$(dir $<)
	$(CC) $(CFLAGS) -c $< -o $@

# Build rule for the final executable
all: $(C_OBJS)
	mkdir -p $(OBJ_DIR)
	$(CC) $(C_OBJS) $(CFLAGS) $(LDFLAGS) -o $(BIN_DIR)/$(OBJ_NAME)

# Run the executable
run: all
	./bin/$(OBJ_NAME)

# Clean up object files and the executable
clean:
	rm -rf $(OBJ_DIR)/* $(BIN_DIR)/$(OBJ_NAME)

# Run tests
test:
	# Add your test command here

# Mark phony targets
.PHONY: all run clean test

