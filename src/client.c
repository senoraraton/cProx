#define _XOPEN_SOURCE 600 // https://stackoverflow.com/a/16401058

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include "client.h"

#define MAX_BUFFER_SIZE 1024

const char* preFlightModification(const char* request) {
    return request;
}
const char* postFlightModification(const char* request) {
    return request;
}

const char* proxyRequestWrapper(int browserfd) {
    char clientReqBuffer[MAX_BUFFER_SIZE];
    char serverReqBuffer[MAX_BUFFER_SIZE];
    ssize_t bytesReceived;
    const char* resp;
    char domain[256], protocol[5];
    int remotefd;

    bytesReceived = recv(browserfd, clientReqBuffer, sizeof(clientReqBuffer) - 1, 0);

    if (bytesReceived <= 0) {
        perror("recv");
    }
    clientReqBuffer[bytesReceived] = '\0';

    resp = preFlightModification(clientReqBuffer);

    sscanf(resp, "GET http://%255[^/ ]/%*s %4s", domain, protocol);
    remotefd = initRemoteConnection(domain, protocol);

    ssize_t bytesSent = send(remotefd, resp, strlen(resp), 0);
    if (bytesSent == -1) {
        perror("Send response failed.");
    }

    bytesReceived = recv(remotefd, serverReqBuffer, sizeof(serverReqBuffer), 0);
    if (bytesReceived == -1) {
        perror("Send response failed.");
    }
    serverReqBuffer[bytesReceived] = '\0';

    resp = postFlightModification(serverReqBuffer);

    close(remotefd);
    return resp;
}

int initRemoteConnection(const char *domain, const char* proto) {
    struct addrinfo hints, *res;
    int status = 0, bind_err = 0, con_err = 0;
    int sockfd;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    if ((status = getaddrinfo(domain, proto, &hints, &res)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        exit(EXIT_FAILURE);
    }

    if ((sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol)) == -1) {
        perror("socket");
        exit(EXIT_FAILURE);
    }

    if ((con_err = connect(sockfd, res->ai_addr, res->ai_addrlen)) != 0) {
        fprintf(stderr, "bind failed: %s\n", gai_strerror(bind_err));
        exit(EXIT_FAILURE);
    }

   freeaddrinfo(res);
   return (sockfd);
}
