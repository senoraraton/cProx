#ifndef HTTP_H 
#define HTTP_H 

#define HTTP_PORT "80"
#define HTTPS_PORT "443"
#define MAX_HEADER_NAME 100
#define MAX_HEADER_VALUE 100
#define MAX_BODY_SIZE 1024

struct HTTPHeader {
    char name[MAX_HEADER_NAME];
    char value[MAX_HEADER_VALUE];
    struct HTTPHeader* next;
};

struct HTTPResponse{
   char method[10];
   char uri[255];
   char version[20];
   char body[MAX_BODY_SIZE];
   struct HTTPHeader* headers;
};

const char* handleHTTPRequest(int newfd);
void parseHTTPRequest(const char *req, struct HTTPResponse* resp);
void buildHTTPRequest(struct HTTPResponse*, char* req);
const char* sendHTTPRequest(int newfd, const char *req);

#endif
