#ifndef CLIENT_H
#define CLIENT_H

const char* proxyRequestWrapper(int browserfd);
int initRemoteConnection(const char *domain, const char* proto);
const char* preFlightModification(const char* requestBuffer);
const char* postFlightModification(const char* requestBuffer);
#endif
