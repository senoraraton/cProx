#include "server.h"

#define BACKLOG 20
#define LISTEN_PORT "1337"
#define MAX_BUFFER_SIZE 1024


int main() {
    init_server();
    return 0;
}
