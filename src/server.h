#ifndef SERVER_H 
#define SERVER_H

void init_server();
void* monitor_thread(void *arg);

#endif
