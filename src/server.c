#define _XOPEN_SOURCE 600 // https://stackoverflow.com/a/16401058

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/wait.h>
#include <pthread.h>

#include "client.h"

#define BACKLOG 20
#define LISTEN_PORT "1337"

struct ThreadArgs {
    int newfd;
    pid_t child_pid;
};

static void *monitor_thread(void *arg) {
    struct ThreadArgs *thread_args = (struct ThreadArgs *)arg;
    int newfd = thread_args->newfd;
    pid_t child_pid = thread_args->child_pid;

    int status;
    waitpid(child_pid, &status, 0);
    close(newfd);
    free(thread_args);
    pthread_exit(NULL);
}

void init_server() {
    struct addrinfo hints, *res;
    struct sockaddr_storage their_addr;
    socklen_t addr_size;
    int status, bind_err, listen_err;
    int sockfd, newfd;
    pthread_t thread_id;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if ((status = getaddrinfo(NULL, LISTEN_PORT, &hints, &res)) != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(status));
        exit(EXIT_FAILURE);
    }

    sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);

    int yes = 1;
    if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) == -1) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    if ((bind_err = bind(sockfd, res->ai_addr, res->ai_addrlen)) != 0) {
        fprintf(stderr, "bind failed: %s\n", gai_strerror(bind_err));
        exit(EXIT_FAILURE);
    }

    if ((listen_err = listen(sockfd, BACKLOG)) != 0) {
        fprintf(stderr, "Listen failed: %s\n", gai_strerror(listen_err));
        exit(EXIT_FAILURE);
    }

    addr_size = sizeof their_addr;
    while (1) {
        newfd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);

        if (newfd == -1) {
            perror("accept");
            continue;
        }

        pid_t pid = fork();

        if (pid == -1) {
            perror("fork");
            close(newfd);
            continue;
        }

        if (pid == 0) {
            const char* res = proxyRequestWrapper(newfd);
            send(newfd, res, strlen(res), 0);
            return;
        } else if (newfd != 0) {
            
            struct ThreadArgs *thread_args = malloc(sizeof(struct ThreadArgs));
            thread_args->newfd = newfd;
            thread_args->child_pid = pid;

            if (pthread_create(&thread_id, NULL, monitor_thread, (void *)thread_args) != 0) {
                perror("pthread_create");
                continue;
            }
            pthread_detach(thread_id);
            newfd = 0;
        }
    }
    freeaddrinfo(res);
}
