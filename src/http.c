#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <stdlib.h>
#include "http.h"

#define MAX_REQUEST_SIZE 1024

void addHeader(struct HTTPHeader** headers, const char* name, const char* value) {
    struct HTTPHeader* newHeader = (struct HTTPHeader*)malloc(sizeof(struct HTTPHeader));
    if (newHeader) {
        strncpy(newHeader->name, name, MAX_HEADER_NAME - 1);
        strncpy(newHeader->value, value, MAX_HEADER_VALUE - 1);
        newHeader->next = *headers;
        *headers = newHeader;
    }
}

void parseHTTPRequest(const char *req, struct HTTPResponse* resp) {
    sscanf(req, "%s %s %s\r\n", resp->method, resp->uri, resp->version);
    printf("Method: %s\n",resp->method);
    printf("URI: %s\n", resp->uri);
    printf("HTTP Version: %s\n", resp->version);

    const char* headerStart = strstr(req, "\r\n") + 2;
    const char* bodyStart = strstr(headerStart, "\r\n\r\n") + 4;

     if (bodyStart > headerStart) {
        const char* headerEnd = bodyStart - 4;
        while (headerStart < headerEnd) {
            char name[MAX_HEADER_NAME], value[MAX_HEADER_VALUE];
            sscanf(headerStart, "%[^:]: %[^\r\n]\r\n", name, value);
            addHeader(&(resp->headers), name, value);
            headerStart = strstr(headerStart, "\r\n") + 2;
        }
    }

    if (*bodyStart != '\0') {
        strncpy(resp->body, bodyStart, MAX_BODY_SIZE - 1);
    }
}

void buildHTTPRequest(struct HTTPResponse* response,char* req) {
    snprintf(req, MAX_REQUEST_SIZE, "%s %s %s\r\n", response->method, response->uri, response->version);

    struct HTTPHeader* currentHeader = response->headers;
    while (currentHeader != NULL) {
        snprintf(req+ strlen(req), MAX_REQUEST_SIZE - strlen(req), "%s: %s\r\n", currentHeader->name, currentHeader->value);
        currentHeader = currentHeader->next;
    }
    snprintf(req + strlen(req), MAX_REQUEST_SIZE - strlen(req), "\r\n");
    snprintf(req + strlen(req), MAX_REQUEST_SIZE - strlen(req), "%s", response->body);
}

